import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.Exception;
import java.text.DateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Dimension;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArvatoDriver extends FirefoxDriver{
	
	boolean activelog=true;
	int month=Calendar.getInstance().get(Calendar.MONTH)+1;
	String daterepository=Calendar.getInstance().get(Calendar.YEAR) +"-" +month+"-" + Calendar.getInstance().get(Calendar.DAY_OF_MONTH)+File.separator;
	String logFileName="system.log";
	String screenPrefixName="";
	String cannot =" cannot be found";
	String screenshots = "screenshots"+File.separator;
	String logs = "logs"+File.separator;
	String numberBuild="";

	public ArvatoDriver(FirefoxProfile profile)
	{
		super(profile);
		this.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
	}
	public ArvatoDriver()
	{
		super();
		this.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
	}	
	public void setSize(int a,int b)
	{
		this.manage().window().setSize(new Dimension(a,b));
	}
	private void clear(String a)
	{
		if(isPresent(a))
		{
				if(activelog)
					this.write(logFileName,"input "+a+" cleared");
				System.out.println("input "+a+" cleared");
				this.findElement(By.cssSelector(a)).clear();
		}
		else
		{
			if(activelog)
			{
				this.screen();
				this.write(logFileName,a+cannot);
				this.closedriver();
			}
		}
    	return ;
	}
	private void clearxpath(String a)
	{
		if(isPresentxpath(a))
		{
				if(activelog)
					this.write(logFileName,"input "+a+" cleared");
				System.out.println("input "+a+" cleared");
				this.findElement(By.xpath(a)).clear();
		}
		else
		{
			if(activelog)
			{
				this.screen();
				this.write(logFileName,a+cannot);
				this.closedriver();
			}
		}
    	return ;
	}
	private boolean isPresent(String a) 
	{
		if(!this.findElements(By.cssSelector(a)).isEmpty())
			return true;
		return false;
	}
	private boolean isPresentxpath(String a) 
	{
		if(!this.findElements(By.xpath(a)).isEmpty())
			return true;
		return false;
	}

	private void erreur()
	{
		this.findElement(By.cssSelector("#NeverExist")).click();
	}
	public void assertUrl(String s)
	{
		if(activelog)
		{
			this.write(this.logFileName,"test if url is equal to ("+s+")");
			System.out.println("test if url is equal to ("+s+")");
		}
		if (!this.getCurrentUrl().equals(s))
		{
			if(activelog){
				this.write(logFileName,"url not equal to "+s);
				this.screen();
				this.erreur();
			}
			System.out.println("url not equal to "+s);
			this.closedriver();
		}
    	return ;
	}
	
	public void assertExists(String s)
	{
		if(activelog)
		{
			this.write(this.logFileName,"test if ("+s+") exists");
			System.out.println("test if ("+s+") exists");
		}
		if(!isPresent(s))
		{
			if(activelog){
				this.write(logFileName,s+cannot);
				this.screen();
				this.erreur();
			}
			this.closedriver();
		}
    	return ;
	}
	public void assertTitle(String s)
	{
		if(activelog)
		{
			this.write(this.logFileName,"test if Title is equal to ("+s+")");
			System.out.println("test if Title is equal to ("+s+")");
		}
		if (!this.getTitle().equals(s))
		{
			if(activelog){
				this.write(logFileName,"Title not equal to "+s);
				this.screen();
				this.erreur();
			}
			System.out.println("Title not equal to "+s);
			this.closedriver();
		}
    	return ;
	}
	public void assertVisible(String s)
	{
		if(activelog)
		{
			this.write(this.logFileName,"test if ("+s+") is visible");
			System.out.println("test if ("+s+") is visible");
		}
		if(!this.findElement(By.cssSelector(s)).isDisplayed())
		{
			if(this.activelog)
			{
				this.write(this.logFileName,s+" not visible");
				this.screen();
				this.erreur();
			}
			System.out.println(s+" not visible");
			this.closedriver();
		}
	}
	public void assertNotVisible(String s)
	{
		if(activelog)
		{
			this.write(this.logFileName,"test if ("+s+") is not visible");
			System.out.println("test if ("+s+") is not visible");
		}
		if(this.findElement(By.cssSelector(s)).isDisplayed())
		{
			if(this.activelog)
			{
				this.write(this.logFileName,s+" visible");
				this.screen();
				this.erreur();
			}
			System.out.println(s+" visible");
			this.closedriver();
		}
	}

	public void fill(String a,String value)
	{
		if(isPresent(a)){
			this.clear(a);
			if(activelog)
				this.write(logFileName,"value "+value+" in "+a);
			System.out.println("value "+value+" in "+a);
			this.findElement(By.cssSelector(a)).sendKeys(value);
		}	
		else{
			this.write(logFileName,a+cannot);
			this.screen();
			this.closedriver();
		}
    	return ;
	}
	public void fillxpath(String a,String value)
	{
		if(isPresentxpath(a)){
			this.clearxpath(a);
			if(activelog)
				this.write(logFileName,"value "+value+" in "+a);
			System.out.println("value "+value+" in "+a);
			this.findElement(By.xpath(a)).sendKeys(value);
		}	
		else{
			this.write(logFileName,a+cannot);
			this.screen();
			this.closedriver();
		}
    	return ;
	}
	public void hover(String a)
	{
	       WebElement a1 = this.findElement(By.cssSelector(a));
	       Actions builder = new Actions(this);
	       builder.moveToElement(a1).perform();
	}
	public void setScreenPrefixName(String a) 
	{
		screenPrefixName=a;
		if(logFileName.contains(File.separator)){
			File dir = new File(screenshots+daterepository+screenPrefixName.substring(0,screenPrefixName.lastIndexOf(File.separator)));
			dir.mkdirs();
		}
		else{
			File dir = new File(screenshots+daterepository);
			dir.mkdirs();
		}
    	return ;
	}
	public void setlogFileName(String a) 
	{
		this.logFileName=a;
		if(logFileName.contains(File.separator)){
			File dir = new File(logs+daterepository+logFileName.substring(0,logFileName.lastIndexOf(File.separator)));
			dir.mkdirs();
		}
		else{
			File dir = new File(logs+daterepository);
			dir.mkdirs();
		}
		if(activelog)
			this.write(logFileName,"[START]");
		System.out.println("[START]");
		
    	return ;
	}
	public void setlog(boolean a)
	{
		this.activelog=a;
    	return ;
	}

	public void open(String url)
	{
		if(activelog)
			this.write(logFileName,url+" --> open");
		System.out.println(url+" --> open");
		this.get(url);
		return ;
	}
	
	public String getPersonalizedField(String name)
	{
		InputStream ips;
		InputStream ips2;
		try {
			String ligne;
			String file = System.getProperty("user.dir").concat(File.separator+"src"+File.separator+"squashTA"+File.separator+"resources"+File.separator+"numberBuild.txt");
			ips = new FileInputStream(file);
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			ligne=br.readLine();
			numberBuild=ligne;
			br.close();
			ips2 = new FileInputStream(numberBuild);
			InputStreamReader ipsr2=new InputStreamReader(ips2);
			BufferedReader br2=new BufferedReader(ipsr2);
			if ((ligne=br2.readLine())!=null){
				Pattern patt=Pattern.compile("\""+name+"\":\"[\\w:/.]*\"");
				Matcher matcher = patt.matcher(ligne);
				if(matcher.find())
				{
					br2.close();
					String value = matcher.group().split("\""+name+"\":")[1];
					return value.substring(1, value.length()-1);
				}			
				br2.close();
			}
		} catch (IOException e) {}
		return null;
	}
	public void click(String a)
	{
		try{
			if(isPresent(a))
			{
				if(activelog)
					this.write(logFileName,"click on "+a);
				System.out.println("click on "+a);
				this.findElement(By.cssSelector(a)).click();
			}
			else{
				this.write(logFileName,a+cannot);
				this.screen();
				this.closedriver();	
			}
		}
		catch(Exception e)
		{
			screen();
		}
    	return ;
	}
	public void clickxpath(String a)
	{
		try{
			if(isPresentxpath(a))
			{
				if(activelog)
					this.write(logFileName,"click on "+a);
				System.out.println("click on "+a);
				this.findElement(By.xpath(a)).click();
			}
			else{
				this.write(logFileName,a+cannot);
				this.screen();
				this.closedriver();	
			}
		}
		catch(Exception e){
			screen();
		}
    	return ;
	}	
	public void log(String s)
	{
		System.out.println(s);
		this.write(logFileName,s);
	}
    private void write(String f, String s)
    {
      try
      {
		  FileWriter aWriter = new FileWriter(logs+daterepository+f, true);
    	  Date now = new Date();
    	  DateFormat mediumDateFormat = DateFormat.getDateTimeInstance(
    			  DateFormat.MEDIUM,
   				  DateFormat.MEDIUM);
   		  String currentTime = mediumDateFormat.format(now);
    	  aWriter.write(currentTime + " " + s + "\n");
    	  aWriter.close();
      }
      catch(IOException e){}
  	return ;
    }
    public void screen()
    {
    	File scrFile = this.getScreenshotAs(OutputType.FILE);
    	try {
			File b= new File(screenshots+daterepository+screenPrefixName+"_"+Calendar.getInstance().get(Calendar.HOUR)+"_"+Calendar.getInstance().get(Calendar.MINUTE)+"_"+Calendar.getInstance().get(Calendar.SECOND)+"_"+Calendar.getInstance().get(Calendar.MILLISECOND)+".jpg");
    		FileUtils.copyFile(scrFile, b);
			if(activelog)
				this.write(this.logFileName,"screen " + b.getAbsolutePath() +" taken");
			System.out.println("screen " + b.getAbsolutePath() +" taken");
			this.closedriver();
    	} catch (IOException e) {}
    	return ;
    }
    public void select(String a,String s)
    {
    	if(isPresent(a))
    	{
    		if(activelog)
    			this.write(logFileName,s +" selected in "+a);
			System.out.println(s +" selected in "+a);
    		new Select(this.findElement(By.cssSelector(a))).selectByVisibleText(s);
    	}
		else{
			this.write(logFileName,a+cannot);
			this.screen();
			this.closedriver();	
		}
    	return ;
    }
    public void selectxpath(String a,String s)
    {
    	if(isPresentxpath(a))
    	{
    		if(activelog)
    			this.write(logFileName,s +" selected in "+a);
			System.out.println(s +" selected in "+a);
    		new Select(this.findElement(By.xpath(a))).selectByVisibleText(s);
    	}
		else{
			this.write(logFileName,a+cannot);
			this.screen();
			this.closedriver();	
		}
    	return ;
    }
    
    public void waitForAjaxResponse()
    {
    	Boolean isJqueryUsed = (Boolean) this.executeScript("return (typeof(jQuery) != 'undefined')");
    	if(isJqueryUsed){
    	  while (true){
    	    Boolean ajaxIsComplete = (Boolean)(this.executeScript("return jQuery.active == 0"));
    	    if (ajaxIsComplete) 
    	    	break;
    	    try{
    	    	Thread.sleep(100);
    	    }
    	    catch (InterruptedException e) {}
    	  }
    	}
    }
    
    public void closedriver()
    {	
    	if(activelog)
    		this.write(logFileName,"[END]");
    	System.out.println("[END]");
    	this.close();
    }
    
}
