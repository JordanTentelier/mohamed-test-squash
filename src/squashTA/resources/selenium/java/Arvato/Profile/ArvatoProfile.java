import org.openqa.selenium.firefox.FirefoxProfile;


public class ArvatoProfile extends FirefoxProfile{
	
	public ArvatoProfile()
	{
		super();
		this.setPreference("network.proxy.type", 1);
		this.setPreference("network.proxy.http", "proxy.arvato.fr");
		this.setPreference("network.proxy.http_port", 8080);
		this.setPreference("network.proxy.ssl", "proxy.arvato.fr");
		this.setPreference("network.proxy.ssl_port", 8080);
		this.setPreference("network.proxy.no_proxies_on", "172.17.241.36");
	}
}
