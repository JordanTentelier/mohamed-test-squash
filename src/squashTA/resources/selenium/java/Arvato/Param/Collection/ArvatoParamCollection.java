import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ArvatoParamCollection{
	List<ArvatoParam> arvatoParam=new ArrayList<ArvatoParam>();
	String string="";
	public ArvatoParamCollection(ArrayList<ArvatoParam> a)
	{
		arvatoParam=a;
	}
	
	public ArvatoParamCollection() {
	}

	public String print()
	{
		Iterator<ArvatoParam> it = arvatoParam.iterator();
		while(it.hasNext())
		{
			ArvatoParam next=it.next();
			if(next.value.equals(""))
			{
				string += "{"+next.key+"{";
				Iterator<ArvatoParam> it2 = next.arvatoColl.arvatoParam.iterator();
				while(it2.hasNext())
				{
					ArvatoParam next2=it2.next();
					string+="{"+next2.key+","+next2.value+"},\n";
				}
				string += "}}";
			}
			else
				string+="{"+next.key+","+next.value+"}\n ";
		}	
		return string;
	}
}
