import java.util.ArrayList;

public class ArvatoParam {
	
	public String key;
	public String value;
	public ArvatoParamCollection arvatoColl;
	
	public ArvatoParam(String a,String b)
	{
		key=a;
		value=b;
		arvatoColl=null;
	}
	public ArvatoParam(String a,ArvatoParamCollection b)
	{
		key=a;
		arvatoColl=b;
		value="";
	}
	public ArvatoParam(String a,ArrayList<ArvatoParam> b)
	{
		key=a;
		arvatoColl = new ArvatoParamCollection(b);
		value="";
	}
	public String print()
	{
		return key+":"+value;
	}
}
