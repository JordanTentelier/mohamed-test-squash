import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import org.w3c.dom.Node;



public class ArvatoSoap_Magento1{
	
	private SOAPMessage soapMessage;
	private MessageFactory messageFactory;
	private SOAPConnection soapConnection;
	private String url;
	String SessionId="";
	private int flag=0;
	boolean noResponse=false;
	ArrayList<ArrayList<ArvatoParam>> list = new ArrayList<ArrayList<ArvatoParam>>();
	ArrayList<ArvatoParam> listparam=new ArrayList<ArvatoParam>();
	
	public ArvatoSoap_Magento1(String url,String log,String mdp)
	{
		this.url=url;
		try
		{	
			System.setProperty("https.proxyHost","proxy.arvato.fr");
			System.setProperty("https.proxyPort","8080");
		 	soapConnection = SOAPConnectionFactory.newInstance().createConnection();
			messageFactory = MessageFactory.newInstance();
			soapMessage = messageFactory.createMessage();
			soapMessage.getSOAPPart().getEnvelope().setAttribute("xmlns:urn","urn:Magento");
			SOAPElement function = soapMessage.getSOAPPart().getEnvelope().getBody().addChildElement("login","urn");
			function.addChildElement("username").addTextNode(log);
			function.addChildElement("apiKey").addTextNode(mdp);
			soapMessage.saveChanges();
			login();
		}
		catch (Exception e) {}		
	}

	public void Struct(String a,ArvatoParamCollection b){
				try {
					soapMessage = messageFactory.createMessage();
					soapMessage.getSOAPPart().getEnvelope().setAttribute("xmlns:urn","urn:Magento");
					SOAPElement function = soapMessage.getSOAPPart().getEnvelope().getBody().addChildElement(a,"urn");
					function.addChildElement("sessionId").addTextNode(SessionId);
					parcoursCollection(b,function);
					soapMessage.saveChanges();
				} catch (SOAPException e) {}

	}
	private void parcoursCollection(ArvatoParamCollection arvatoColl,SOAPElement element)
	{
		for(ArvatoParam param : arvatoColl.arvatoParam)
		{
				try {
					if(param.value.equals(""))
					{
						SOAPElement function2;
						function2 = element.addChildElement(param.key);
						parcoursCollection(param.arvatoColl,function2);
					}
					else
					{
						element.addChildElement(param.key).addTextNode(param.value);
					}
				} catch (SOAPException e) {}
		}
	}
	public void sendSoap()
	{
		try {
			InterpreteResponse(soapConnection.call(soapMessage,url));
		} catch (SOAPException e) {}
	}
	private void InterpreteResponse(SOAPMessage a)
	{
				try {
					if(a.getSOAPPart().getEnvelope().getBody().hasFault())
					{
						if(a.getSOAPPart().getEnvelope().getBody().getElementsByTagName("faultcode").item(0).getTextContent().equals("5"))
						{
							if(flag<1)
							{
								System.out.println("Vous avez été déconnecté, nous vous reconnectons");
								flag++;
								login();
								sendSoap();
							}
						}
					}
					else
					{	
						SOAPBody soapBody = a.getSOAPPart().getEnvelope().getBody();
						Node SoapResponse = soapBody.getChildNodes().item(1).getChildNodes().item(1);
						if(SoapResponse.getFirstChild()!=null)
							{
							GetChildren(SoapResponse);
							noResponse=false;
							}
						else
							noResponse=true;
					}
				} catch (SOAPException e) {}
	}
	private void GetChildren(Node soapResponse)
	{
		Node nextChild ;
		if(soapResponse.getFirstChild().getNextSibling()!=null)
		{
			for(Node childNode = soapResponse.getFirstChild(); childNode!=null;)
			{

				nextChild= childNode.getNextSibling();
				if(childNode.getNodeType()== Node.ELEMENT_NODE)
				{
					GetChildren(childNode);
					if(childNode.getFirstChild().getNextSibling()!=null){
						write("listparam clear");
						list.add(listparam);
						listparam=new ArrayList<ArvatoParam>();
					}
				}
			    childNode = nextChild;
			}
		}
		else
		{
			if(soapResponse.getNodeType()==Node.ELEMENT_NODE)
			{
				write("ajout de "+soapResponse.getNodeName() +" : "+soapResponse.getTextContent());
				listparam.add(new ArvatoParam(soapResponse.getNodeName(),soapResponse.getTextContent()));
			}
			nextChild = soapResponse.getNextSibling();
			if(nextChild!=null && nextChild.getNodeType()== Node.ELEMENT_NODE)
			{
				GetChildren(nextChild);
			}
		}
	}
	@SuppressWarnings("unused")
	private void parcoursParam(ArrayList<ArvatoParam> a)
	{
		System.out.println("debut param");
		for(ArvatoParam ab : a)
		{
			System.out.println(ab.key +" : "+ab.value);
		}
		System.out.println("fin param\n");
	}
	private void login()
	{
		try {
			if(flag<=1)
				getSessionIdAfterLogin(soapConnection.call(soapMessage,url));
		} catch (Exception e) {}
	}
    private void getSessionIdAfterLogin(SOAPMessage soapResponse) throws Exception{
    	SessionId = soapResponse.getSOAPPart().getEnvelope().getBody().getElementsByTagName("loginReturn").item(0).getTextContent();
    }
    
	private void write(String f)
	    {
	      try
	      {
			  FileWriter aWriter = new FileWriter("debug1234.log", true);
	    	  aWriter.write(f + "\n");
	    	  aWriter.close();
	      }
	      catch(IOException e){}
	  	return ;
	    }
}
